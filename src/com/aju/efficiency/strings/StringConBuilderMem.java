package com.aju.efficiency.strings;

public class StringConBuilderMem extends Strings {
	
	public StringConBuilderMem(String cad, int num) {
		super(cad, num);
	}
	
	/**
	 * Metodo que concatena N cadenas usando la clase StringBuilder con tamaño reservado en memoria
	 * @author Alvaro J Urbaez 
	 */
	@Override
    void concatenar() {
    	StringBuilder resultado = new StringBuilder(cad.length()*num);
        long t1, t2;
        System.out.println("[4] Concatenando "+num+" cadenas ("+cad+") con StringBuilder con tamaño reservado en memoria.");
        t1 = System.currentTimeMillis();
        for (int i = 1; i < num; i++) {
        	resultado.append(cad);
        }
        t2 = System.currentTimeMillis();
        System.out.println("[4] Completado en "+(t2-t1)+" milisegundos");
        result = resultado.toString();
    }
}
