package com.aju.efficiency.strings;

/**
 * Clase principal para probar la eficiencia de los diferentes metodos de 
 * concatenacion de Strings
 * @author Alvaro J Urbaez
 */
public class StringsMain {
	
	public static void main(String[] args) {
		System.gc();
		int iter = 1000000;
		String cad = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse porta elementum efficitur.";
		new Thread(new StringConMas(cad, iter/100)).start();
		new Thread(new StringConcat(cad, iter)).start();
		new Thread(new StringConBuilder(cad, iter)).start();
		new Thread(new StringConBuilderMem(cad, iter)).start();
		System.out.println("Fin del main...");
	}
}
