package com.aju.efficiency.strings;

public class StringConMas extends Strings {
	
    public StringConMas(String cad, int num) {
		super(cad, num);
	}

    /**
     * Metodo que concatena N cadenas usando la clase String y el operador (+)
     * @author Alvaro J Urbaez
     */
	@Override
    void concatenar() {
        result = cad;
        long t1, t2;
        System.out.println("[1] Concatenando "+num+" cadenas ("+cad+") con String y el operador (+).");
        t1 = System.currentTimeMillis();
        for (int i = 1; i < num; i++) {
            result += cad;
        }
        t2 = System.currentTimeMillis();
        System.out.println("[1] Completado en "+(t2-t1)+" milisegundos");
    }
}
