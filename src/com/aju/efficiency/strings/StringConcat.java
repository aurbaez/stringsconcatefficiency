package com.aju.efficiency.strings;

public class StringConcat extends Strings {
	
	public StringConcat(String cad, int num) {
		super(cad, num);
	}
	    
    /**
     * Metodo que concatena N cadenas usando la clase String y su metodo concat()
     * @author Alvaro J Urbaez
     */
	@Override
    void concatenar() {
        result = cad;
        long t1, t2;
        System.out.println("[2] Concatenando "+num+" cadenas ("+cad+") con String y su método concat().");
        t1 = System.currentTimeMillis();
        for (int i = 1; i < num; i++) {
            result.concat(cad);
        }
        t2 = System.currentTimeMillis();
        System.out.println("[2] Completado en "+(t2-t1)+" milisegundos");
    }
}
