package com.aju.efficiency.strings;

public abstract class Strings implements Runnable {
	
	String cad = "mm-nn-pp-";
	int num = 0;
	String result = null;
	
	Strings(String cad, int num) {
		this.cad = cad;
		this.num = num;
	}
	
	/**
	 * Metodo que concatena N cadenas segun cada implementacion
	 * @author Alvaro J Urbaez
	 */
    abstract void concatenar();

	@Override
	public void run() {
		concatenar();
	}
	
	public String getResult() {
		return this.result;
	}
}
