package com.aju.efficiency.strings;

public class StringConBuilder extends Strings {
	
	public StringConBuilder(String cad, int num) {
		super(cad, num);
	}
	
	/**
	 * Metodo que concatena N cadenas usando la clase StringBuilder y su metodo append()
	 * @author Alvaro J Urbaez 
	 */
	@Override
    void concatenar() {
        StringBuilder resultado = new StringBuilder(cad);
        long t1, t2;
        System.out.println("[3] Concatenando "+num+" cadenas ("+cad+") con StringBuilder y su metodo append().");
        t1 = System.currentTimeMillis();
        for (int i = 1; i < num; i++) {
        	resultado.append(cad);
        }
        t2 = System.currentTimeMillis();
        System.out.println("[3] Completado en "+(t2-t1)+" milisegundos");
        result = resultado.toString();
    }
}
